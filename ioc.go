package ioc

import (
	"fmt"
	"reflect"
)

// Creater is a factory with arbetrary arguments and return value(s).
//
// It will be used reflection to determine which dependency the created struct has.
//
// To be a valid Creater implementations of this interface must implement a "IoCCreate" function.
// The create function can have arbetrary parameters, which are seen as dependencies for the return value.
type Creater interface {
	// IsIoCCreater is a guard method, that ensures that a struct
	// willingly implements the Creater interface. Not just by
	// "chance" have methods with the same name.
	IsIoCCreater() bool
	// GetIoCName returns the name of this factory.
	// The name can then be used to ease debugging.
	GetIoCName() string
	// GetIoCBehaviour returns the Behaviour for this Creater.
	// See doc for Behaviour for details.
	GetIoCBehaviour() Behaviour
	// GetIoCContexts returns the contexts this Creater should be
	// used in.
	//
	// If the returned array is empty or nil, the creater will
	// be used in any Contexts, unless there is a Creater that
	// matches the active context(s).
	//
	// This method may only be called once by a Container.
	// Hence the contexts this Creater is used in may not be
	// changeable at a later point.
	GetIoCContexts() []string
}

// CreaterMethodName is the name of the method expected in a Creater.
//
// This name will be used by reflect.Type.MethodByName.
const CreaterMethodName = "IoCCreate"

var defaultContainer = NewContainer()

var defaultIsLazy = false
var defaultBehaviour = BSingleton

// NewContainer will return a new Container instance.
//
// Currently this method will return a default Container implementation, but this may change as this library
// progresses.
func NewContainer() Container {
	// Do not use "field:value" syntax, to get compiler error when missing fields
	return &DefaultContainer{
		make(map[reflect.Type]*DependencyNode), // AllNodes
		make(map[reflect.Type]*DependencyNode), // RootNodes
		[]*DependencyNode{},                    // IniterNodes
		false,                                  // isInit
		true,                                   // isActive
		defaultIsLazy,                          // isLazy
		defaultBehaviour,                       // DefaultBehaviour
		[]string{},                             // ActiveContexts
	}
}

// Register will add the given object to the default IoC container.
//
// Currently this method supports Creaters, which will act as factories for instances, functions used as factory method
// with less available meta data, and instances (which match none of the preivious types). If an instance is given,
// it will be used as a singleton in the dependency graph.
func Register(any interface{}) (interface{}, error) {
	return defaultContainer.Register(any)
}

// RegisterCreater will add a Creater to the default container.
//
// The Creater's IoCCreate method will be used to determine which instances are
// required (parents), and which instances are created (children). All those nodes
// will then be added to the dependency graph.
//
// If registration fails for any parent or child, the dependency graph will not be
// changed.
func RegisterCreater(c Creater) (Creater, error) {
	return defaultContainer.RegisterCreater(c)
}

// RegisterInstance will add the instance to the default container.
//
// The given instance will be used as a singleton for every subsequent get call.
//
// If nil was given, or instance's type is already registered to the
// dependency graph, an error will be returned.
//
// The given instance will always be the first return value.
func RegisterInstance(i interface{}) (interface{}, error) {
	return defaultContainer.RegisterInstance(i)
}

// MustRegister does the same as Register, but panics if the Creater could not be registerd.
func MustRegister(any interface{}) interface{} {
	return defaultContainer.MustRegister(any)
}

// MustRegisterCreater does the same as RegisterCreater, but panics if the creater could not be registered.
func MustRegisterCreater(c Creater) Creater {
	return defaultContainer.MustRegisterCreater(c)
}

// MustRegisterInstance does the same as RegisterInstance, but panics if the instance could not be registered.
func MustRegisterInstance(i interface{}) interface{} {
	return defaultContainer.MustRegisterInstance(i)
}

// RegisterFunc will call RegisterFunc of the default container.
func RegisterFunc(i interface{}) (interface{}, error) {
	return defaultContainer.RegisterFunc(i)
}

// MustRegisterFunc does the same as RegisterFunc, but panics if it fails.
func MustRegisterFunc(i interface{}) interface{} {
	return defaultContainer.MustRegisterFunc(i)
}

// Init will initialize the default container.
//
// After initialization was successful, all nodes are instanciated and types can be retrieved immediately.
func Init() error {
	return defaultContainer.Init()
}

// GetChannel returns a channel that will publish the instance once it is ready.
//
// If the instance is never created, the channel will publish an error.
//
// See Init function.
func GetChannel(i interface{}) chan interface{} {
	return defaultContainer.GetChannel(i)
}

// Get returns an instance of given instance type. When the container is ready, the instance will be set to the
// real value. Until then, the returned value is equal to the zero value.
//
// The given value may be a casted nil, but must be a pointer type.
func Get(i interface{}) (interface{}, error) {
	return defaultContainer.Get(i)
}

// MustGetChannel does the same as GetChannel, but panics if getting fails
func MustGetChannel(i interface{}) chan interface{} {
	return defaultContainer.MustGetChannel(i)
}

// MustGet does the same as Get, but panics if getting fails.
func MustGet(i interface{}) interface{} {
	return defaultContainer.MustGet(i)
}

// GetNow tries to retrieve an instance immediately instead of waiting
// for initialization.
//
// If the node does not exist yet or the instance cannot be retrieved,
// a zero value is returned.
func GetNow(i interface{}) interface{} {
	return defaultContainer.GetNow(i)
}

// MustInit does the same as Init, but panics if it fails.
func MustInit() {
	defaultContainer.MustInit()
}

func printAllNodes() {
	fmt.Printf("%+v\n", defaultContainer.(*DefaultContainer).Nodes)
}
