package ioc

type oneDependency struct {
	dep *dependency
}

type dependency struct {
	Name string
}

func (t oneDependency) IsIoCCreater() bool {
	return true
}

func (t oneDependency) GetIoCName() string {
	return "TestStruct"
}

func (t oneDependency) GetIoCBehaviour() Behaviour {
	return BSingleton
}

func (t oneDependency) GetIoCContexts() []string {
	return []string{}
}

func (t oneDependency) IoCCreate(d1 *dependency) *oneDependency {
	return &oneDependency{
		dep: d1,
	}
}

type duplicatOutPutType struct {
}

type noCreateMethod struct {
}

func (t noCreateMethod) IsIoCCreater() bool {
	return true
}

func (t noCreateMethod) GetIoCName() string {
	return "TestStruct"
}

func (t noCreateMethod) GetIoCBehaviour() Behaviour {
	return BSingleton
}

func (t noCreateMethod) GetIoCContexts() []string {
	return []string{}
}

type createsTwo struct {
}

func (c createsTwo) IsIoCCreater() bool {
	return true
}

func (c createsTwo) GetIoCName() string {
	return "TestStruct"
}

func (c createsTwo) GetIoCBehaviour() Behaviour {
	return BSingleton
}

func (c createsTwo) GetIoCContexts() []string {
	return []string{}
}

type ServiceA struct {
	Name string
}

type ServiceB struct {
	MagicNumber int
}

type dependsOnSelf struct {
}

func (t dependsOnSelf) IsIoCCreater() bool {
	return true
}

func (t dependsOnSelf) GetIoCName() string {
	return "TestStruct"
}

func (t dependsOnSelf) GetIoCBehaviour() Behaviour {
	return BPrototype
}

func (t dependsOnSelf) GetIoCContexts() []string {
	return []string{}
}

func (t dependsOnSelf) IoCCreate(other *dependsOnSelf) *dependsOnSelf {
	return &dependsOnSelf{}
}

type prototypeBehaviourCreater struct {
	num int
}

func (t prototypeBehaviourCreater) IsIoCCreater() bool {
	return true
}

func (t prototypeBehaviourCreater) GetIoCName() string {
	return "TestStruct"
}

func (t prototypeBehaviourCreater) GetIoCBehaviour() Behaviour {
	return BPrototype
}

func (t prototypeBehaviourCreater) GetIoCContexts() []string {
	return []string{}
}

func (t prototypeBehaviourCreater) IoCCreate() *prototypeBehaviourCreater {
	return &prototypeBehaviourCreater{
		42,
	}
}

type StringerImpl struct{}

func (s *StringerImpl) String() string {
	return "Dummy stringer"
}

type singleTonTestStruct struct {
	num int
}

type createrReturningFalse struct {
}

func (c *createrReturningFalse) IsIoCCreater() bool {
	return false
}

func (c *createrReturningFalse) GetIoCName() string {
	return ""
}

func (c *createrReturningFalse) GetIoCBehaviour() Behaviour {
	return BSingleton
}

func (c *createrReturningFalse) GetIoCContexts() []string {
	return []string{}
}

func (c *createrReturningFalse) IoCCreate() struct{} {
	return struct{}{}
}

type createrWithUndefinedBehaviour struct {
}

func (c *createrWithUndefinedBehaviour) IsIoCCreater() bool {
	return true
}

func (c *createrWithUndefinedBehaviour) GetIoCName() string {
	return ""
}

func (c *createrWithUndefinedBehaviour) GetIoCBehaviour() Behaviour {
	return BUndefined
}

func (c *createrWithUndefinedBehaviour) GetIoCContexts() []string {
	return []string{}
}

func (c *createrWithUndefinedBehaviour) IoCCreate() struct{} {
	return struct{}{}
}
