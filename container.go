package ioc

// Container is the interface for IoC Containers.
//
// The main responsibility of a Container is to register, manage, and return instances. In particular, a container
// has to resolve dependencies of instances to other instances. Those dependencies are resolved using reflection.
//
// Dependecies are defined as the input parameters to a create function (either the function given to Register, or
// the create method of Creaters as specified by the const CreaterMethodName). The output(s) of the same function
// are the created instances.
//
// The container has to guarantee, that none of the input parameters are nil values when calling the function. This
// also means, the actual value behind an interface must not be nil.
//
// Depending on the implementation, a container may allow multiple registrations of one and the same type. The default
// implementation of the ioc package does not allow that.
//
// Furthermore, it is not defined, when two types are equal. Especially the case if MyStruct == *MyStruct (in other
// words, if pointers to a struct are equal to the struct itself) is implementation specific. The default implementation
// will treat pointers and struct as equal, and two structs are equal iff both reflect.TypeOf() are equal.
//
// As consequence, it is also not defined, how instances are retrieved from the container. The only thing that must be
// guaranteed is, that the returned value must be castable to the given instance type. So the following line of code
// always has to work:
//    container.GetNow(&WantedType{}).(*WantedType)
//
//
// Example(taken from `example/basicsetup/basic_example.go`):
//     package main
//
//     import (
//     	"fmt"
//
//     	ioc "gitlab.com/Znert/go-ioc"
//     )
//
//     // MyService is a sample service.
//     // We want the container to manage an instance of this service.
//     type MyService struct {
//     	name string
//     }
//
//     // SayHello will print out a Hello World message.
//     func (m *MyService) SayHello() {
//     	fmt.Println("Hello World! May name is", m.name)
//     }
//
//     // Some file (the provider) must register the service to the IoC container
//     var myService = ioc.MustRegister(&MyService{
//     	name: "Ada Lovelace",
//     }).(*MyService)
//
//     // Some consumer wants to have the service as global variable.
//     //
//     // Give a pointer value to the type you want to get.
//     var fromContainer = ioc.MustGet((*MyService)(nil)).(*MyService)
//
//     func main() {
//
//     	// The main thread calls the Init function
//     	ioc.MustInit()
//
//     	// fromContainer is now the registerd instance
//     	fromContainer.SayHello()
//
//     	// Output: Hello World! May name is Ada Lovelace
//     }
type Container interface {
	Register(any interface{}) (interface{}, error)
	MustRegister(any interface{}) interface{}
	RegisterCreater(c Creater) (Creater, error)
	MustRegisterCreater(c Creater) Creater
	RegisterInstance(i interface{}) (interface{}, error)
	MustRegisterInstance(i interface{}) interface{}
	RegisterFunc(i interface{}) (interface{}, error)
	MustRegisterFunc(i interface{}) interface{}

	GetChannel(i interface{}) chan interface{}
	Get(i interface{}) (interface{}, error)
	MustGetChannel(i interface{}) chan interface{}
	MustGet(i interface{}) interface{}
	GetNow(i interface{}) interface{}

	Init() error
	MustInit()
	Stop() error
	Reset() error

	IsLazy() bool
	SetLazy(lazy bool)
	IsActive() bool
	IsInit() bool

	GetDefaultBehaviour() Behaviour
	SetDefaultBehaviour(b Behaviour)

	GetActiveContexts() []string
	AddContext(context string) bool
	Size() int
}
