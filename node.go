package ioc

import (
	"fmt"
	"reflect"
)

// DependencyNode represents a single Type that is handled by a IoC Container.
//
// A node also holds all required meta information about the instance and it's Creater to build a dependency graph.
// The behaviour of the graph can be changed by setting the Behaviour field.
//
// This type is only exported to extend the default implementation of this IoC system. Normal users do not need to
// understand this detail.
type DependencyNode struct {
	// Reference to potential factories for this instance. After the context initialized this may be nil.
	PotentialFactories []*Factory
	// Reference to the factory that creates the instance. Might be nil if instance is given instead of Factory.
	Factory *Factory
	// The instance of this depency node. It is used when getting an instance for a Type.
	Instance interface{}
	// The Type of the instance.
	InstanceType reflect.Type
	// Reference to all nodes that must be initialized to create this instance.
	Parents []*DependencyNode
	// References to all children, who have this node as dependency.
	Children []*DependencyNode
	// How the node behaves on getting the instance.
	Behaviour Behaviour
	// Mark this node as dummy. A dummy is a node that cannot be instanciated, but has dependants.
	IsDummy bool
	// True if this node is initialized and ready for getting the instance
	IsInit bool
	// Specifies what kind of node this node is.
	Type nodeType
	// List of cahnnels to clients that wait for the instantiation of this node.
	WaitingClients []*WaitingClient
	// List of channels to clienst, that must get an instance.
	MustWaitingClients []*WaitingClient
	// Container is a reference to the container this Depedency node is in
	Container Container
	// FactoryIndex defines at which position in the output array this node can be found
	FactoryIndex int
}

// WaitingClient represents a Client that wants to have a specific type.
type WaitingClient struct {
	// The channel the client is waiting on
	ch chan interface{}
	// The type of the puslished value
	requestedType reflect.Type
}

// NewDepNode creates a new, empty DependencyNode.
func NewDepNode() *DependencyNode {
	return &DependencyNode{
		[]*Factory{},        // PotentialFactories
		nil,                 // Factory
		nil,                 // Instance
		nil,                 // InstanceType
		[]*DependencyNode{}, // Parents
		[]*DependencyNode{}, // Children
		BUndefined,          // Behaviour
		false,               // IsDummy
		false,               // IsInit
		NUndefined,          // Type
		[]*WaitingClient{},  // WaitingClients
		[]*WaitingClient{},  // MustWaitingClients
		defaultContainer,    // Container
		-1,                  // FactoryIndex
	}
}

// PickFactory will choose a factory fo this node based on active contexts.
//
// If not exactly one factory can be picked, an error is returned.
func (node *DependencyNode) PickFactory() (r *Factory, e error) {

	if node.Type != NFactory {
		// This node does not use a factory
		return nil, nil
	}

	defer func() {
		if e != nil || r == nil {
			return
		}

		// Calculation was successful, update all affected nodes
		factoryIndex := indexOfOutput(r.Creates, node)
		if factoryIndex == -1 {
			e = fmt.Errorf("Chosen factory %v does not produce node %v", r.Method.Type(), node.InstanceType)
			r = nil
			return
		}

		node.Factory = r
		node.Parents = r.Input
		node.Behaviour = r.Behaviour
		for _, p := range node.Parents {
			// Only add child if is not already added
			if !containsChild(p.Children, node) {
				p.Children = append(p.Children, node)
			}
		}
		node.InstanceType = node.Factory.CreatesTypes[factoryIndex]
	}()

	// TODO: If factory changes, remove this node from all parents

	activeContexts := node.Container.GetActiveContexts()

	// Factory not set, pick from potential factories
	if len(activeContexts) == 0 {
		// No context set, prefer factories with no context
		// If no factory with empty context exist, pick factory with context (if there is only one).
		// Else, return error.
		contextFactories := []*Factory{}
		contextFreeFactoreis := []*Factory{}

		for _, f := range node.PotentialFactories {
			if len(f.Contexts) == 0 {
				contextFreeFactoreis = append(contextFreeFactoreis, f)
			} else {
				contextFactories = append(contextFactories, f)
			}
		}

		if len(contextFreeFactoreis) > 1 {
			return nil, fmt.Errorf("Cannot initialise: Multiple matched factories with no context found for %T with active contexts %v",
				node.InstanceType, activeContexts)
		}

		if len(contextFreeFactoreis) == 1 {
			return contextFreeFactoreis[0], nil
		}
		clen := len(contextFactories)
		if clen == 0 {
			return nil, fmt.Errorf("Cannot initialise: No factories found for %T with active contexts %v",
				node.InstanceType, activeContexts)
		}
		if clen != 1 {
			return nil, fmt.Errorf("Cannot initialise: Multiple matched factories with set context(s) found for %T with active contexts %v",
				node.InstanceType, activeContexts)
		}

		return contextFactories[0], nil
	}

	// There are active contexts set, prefer factories which match at least one of the contexts
	// If no factory with context matches, pick factory without context (if there is only one).
	// Else, return error
	contextFactories := []*Factory{}
	contextFreeFactoreis := []*Factory{}

	activeContextSet := make(map[string]bool)
	for _, context := range activeContexts {
		activeContextSet[context] = true
	}

	for _, f := range node.PotentialFactories {
		if len(f.Contexts) == 0 {
			contextFreeFactoreis = append(contextFreeFactoreis, f)
		} else {
			for _, context := range f.Contexts {
				if activeContextSet[context] {
					contextFactories = append(contextFactories, f)
					break
				}
			}
		}
	}

	if len(contextFactories) > 1 {
		return nil, fmt.Errorf("Cannot initialise: Multiple matched factories with matching context found for %T with active contexts %v",
			node.InstanceType, activeContexts)
	}

	if len(contextFactories) == 1 {
		return contextFactories[0], nil
	}
	clen := len(contextFreeFactoreis)
	if clen == 0 {
		return nil, fmt.Errorf("Cannot initialise: No factories found for %T with active contexts %v",
			node.InstanceType, activeContexts)
	}
	if clen != 1 {
		return nil, fmt.Errorf("Cannot initialise: Multiple matched factories with set context(s) found for %T with active contexts %v",
			node.InstanceType, activeContexts)
	}

	return contextFreeFactoreis[0], nil
}

// CallFactory will call the factory of this node.
//
// If the type of this node is not NFactory, it will return nil, nil immediately.
// If this node has no factory set yet, this method will pick a factory first. When this fails, an error and nil will
// be returned.
func (node *DependencyNode) CallFactory() (result []reflect.Value, err error) {
	if node.Type != NFactory {
		return nil, nil
	}
	if node.Factory == nil {
		_, err := node.PickFactory()
		if err != nil {
			return nil, fmt.Errorf("Cannot call factory: Picking factory failed: %v", err)
		}
	}
	return node.Factory.Call()
}

func (node *DependencyNode) String() string {
	return fmt.Sprintf(
		"DependencyNode{Instance:%v, InstanceType:%v, Type:%v, Factory:%p, Children:%v, Parents:%v}",
		node.Instance,
		node.InstanceType,
		node.Type,
		node.Factory,
		len(node.Children),
		len(node.Parents),
	)
}

// GetInstance will return the instance of this node.
//
// How this instance is retrieved depends on the type of Behaviour used.
// See Behaviour.GetInstance(*DependencyNode) for more details.
func (node *DependencyNode) GetInstance() (interface{}, error) {

	if node.IsDummy {
		return nil, fmt.Errorf("Cannot get instance: Node %v is a dummy node", node.InstanceType)
	}

	if err := node.Init(); err != nil {
		return nil, fmt.Errorf("Cannot get instance: %v", err)
	}

	var i interface{}
	var err error
	if i, err = node.Behaviour.GetInstance(node); err != nil {
		err = fmt.Errorf("Cannot get instance: %v", err)
		return nil, err
	}
	return i, nil
}

// Init will initialize this node.
//
// When Init has finished without error, the node will be ready for operation.
// This means that its factory is set (if needed), and the instance is set to an
// initial value.
//
// This means, that any parents for this node will also have to be initialised.
func (node *DependencyNode) Init() error {
	if node.IsDummy {
		return fmt.Errorf("Cannot initialise node: Node %v is dummy", node.InstanceType)
	}

	if node.IsInit {
		return nil
	}

	if err := node.Type.InitNode(node); err != nil {
		return fmt.Errorf("Failed to init node %v: %v", node.InstanceType, err)
	}

	if node.Behaviour == BDefault {
		node.Behaviour = node.Container.GetDefaultBehaviour()
	}

	return nil
}

// Publish will push this node's value to all waiting clients.
//
// The node must be initialized to publish to clients.
func (node *DependencyNode) Publish() error {

	if !node.IsInit {
		return fmt.Errorf("Node %v not yet initialized", node.InstanceType)
	}

	for _, c := range node.WaitingClients {
		if err := publishToChannel(c, node); err != nil {
			return fmt.Errorf("Failed to publish %v: %v", node.InstanceType, err)
		}
	}
	for _, c := range node.MustWaitingClients {
		if err := publishToChannel(c, node); err != nil {
			return fmt.Errorf("Failed to publish %v: %v", node.InstanceType, err)
		}
	}

	node.WaitingClients = []*WaitingClient{}
	node.MustWaitingClients = []*WaitingClient{}

	return nil
}

// Reset will un-init this node.
//
// This means that IsInit is set to false, the factory is set to nil and the Behaviour is set to
// undefined if this node is of type NFactory.
func (node *DependencyNode) Reset() {
	node.IsInit = false
	node.Factory = nil
	if node.Type == NFactory {
		node.Behaviour = BUndefined
	}
}
