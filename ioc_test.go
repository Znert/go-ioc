package ioc

import (
	"fmt"
	"reflect"
	"testing"
)

var testCounter = 1

func TestIfValidCreaterIsRegistered(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	toRegister := &oneDependency{}

	_, err := Register(toRegister)
	if err != nil {
		t.Fatalf("ioc should have registered TestStruct, but didn't: %v", err)
	}

	node := defaultContainer.(*DefaultContainer).Nodes[reflect.TypeOf(toRegister)]
	if node == nil {
		t.Fatalf("Node was not registerd with key %T", toRegister)
	}
	if node.Behaviour != BUndefined {
		t.Errorf("Node behaviour was not set to %v after register (and before init): %+v", BUndefined, node.Behaviour)
	}
	if len(node.Children) != 0 {
		t.Error("Node has children, although only one registration was done")
	}
	if node.Instance != nil {
		t.Errorf("Instance of node is not nil: %+v", node.Instance)
	}
	if node.Factory != nil {
		t.Errorf("Node has factory registered: %+v", node.Factory)
	}
	if len(node.PotentialFactories) == 0 {
		t.Error("No potential factories registerd")
	} else if len(node.PotentialFactories) != 1 {
		t.Errorf("More than one potential factory was registered! Got %d factories %v", len(node.PotentialFactories), node.PotentialFactories)
	} else {
		factory := node.PotentialFactories[0]
		if factory.Creater != toRegister {
			t.Errorf("Factory does not use given creater! %+v", factory.Creater)
		}
		if len(factory.Creates) == 0 {
			t.Errorf("Factory creates no dependency nodes: %+v", factory)
		} else {
			if factory.Creates[0] != node {
				t.Errorf("Factory has wrong created node: %+v", factory.Creates[0])
			}

			if len(factory.Creates) != 1 {
				t.Errorf("Factory has more than one crated DependencyNode: %+v", factory.Creates)
			}

			if len(factory.Creates) != len(factory.CreatesTypes) {
				t.Errorf("Factory does not have an equal amount of Creates and CreatesTypes:\n%+v\n%+v",
					factory.Creates, factory.CreatesTypes)
			}
		}

		if len(node.PotentialFactories) != 1 {
			t.Errorf("More than one factory registered: %+v", node.PotentialFactories)
		}
	}

	if t.Failed() {
		t.Logf("Full node: %+v", node)
	}
}

func TestIfRegisterReturnsGivenParameter(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	given := &oneDependency{}
	got, _ := Register(given)

	if got != given {
		t.Errorf("ioc.Register returned a differnt value from what was given")
	}
}

func TestRegisterWithNilShouldFail(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	var nilParam *oneDependency

	_, err := Register(nilParam)

	if err == nil {
		t.Fatal("ioc.Register returned no error when nil was given")
	}
}

func TestInitShouldInstanciateDepedencyNode(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	dep := &dependency{"dependency"}

	_, err := Register(dep)

	if err != nil {
		t.Fatalf("Failed registration: %v", err)
	}

	creater := &oneDependency{}

	_, err = Register(creater)

	if err != nil {
		t.Fatalf("Failed registration: %v", err)
	}

	err = Init()

	if err != nil {
		t.Fatalf("Failed initialisation: %v", err)
	}

	node := defaultContainer.(*DefaultContainer).Nodes[reflect.TypeOf(creater)]

	if node == nil {
		t.Fatalf("Failed to get node with type %T from graph", creater)
	}

	if node.Instance == nil {
		t.Fatal("Instance was not created from creater")
	}

	fromIoC, ok := node.Instance.(*oneDependency)

	if !ok {
		printAllNodes()
		t.Fatalf("Created instance is not of type oneDependency (the registered type)! Acutal: %T", node.Instance)
	}

	if fromIoC.dep != dep {
		t.Fatalf("Injected dependency is not equal to the given resource!")
	}
}

func (t duplicatOutPutType) IsIoCCreater() bool {
	return true
}

func (t duplicatOutPutType) GetIoCName() string {
	return "TestStruct"
}

func (t duplicatOutPutType) GetIoCBehaviour() Behaviour {
	return BSingleton
}

func (t duplicatOutPutType) GetIoCContexts() []string {
	return []string{}
}

func (t duplicatOutPutType) IoCCreate() (*duplicatOutPutType, *duplicatOutPutType) {
	return &duplicatOutPutType{}, &duplicatOutPutType{}
}

func TestRegisterWithDuplicateReturnTypeShouldFail(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	c := &duplicatOutPutType{}

	_, err := RegisterCreater(c)

	if err == nil {
		t.Fatal("Register did not return error with invalid input")
	}
}

func TestRegisterWithCreaterWithoutCreateMethodShouldFail(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	c := &noCreateMethod{}

	_, err := RegisterCreater(c)

	if err == nil {
		t.Fatal("Register did not return error with invalid input")
	}
}

var createsTwoCalls = 0

func (c createsTwo) IoCCreate() (*ServiceA, *ServiceB) {
	createsTwoCalls++
	return &ServiceA{"Alice"}, &ServiceB{42}
}

func TestFactoryShouldOnlyBeCalledOnce(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	c := &createsTwo{}
	MustRegisterCreater(c)

	err := Init()

	if err != nil {
		t.Fatalf("Failed to initialize: %v", err)
	}

	if createsTwoCalls != 1 {
		t.Errorf("Create method was called more than once! Call counter: %d", createsTwoCalls)
	}

	typeA := reflect.TypeOf(&ServiceA{})
	node := defaultContainer.(*DefaultContainer).Nodes[typeA]

	if node != nil {
		if len(node.PotentialFactories) != 1 {
			t.Errorf("There should only excatly one potential factory, but got %d: %v", len(node.PotentialFactories), node.PotentialFactories)
		}
	} else {
		t.Errorf("No node matching %v was found", typeA)
	}

	typeB := reflect.TypeOf(&ServiceB{})
	node = defaultContainer.(*DefaultContainer).Nodes[typeB]

	if node != nil {
		if len(node.PotentialFactories) != 1 {
			t.Errorf("There should only excatly one potential factory, but got %d: %v", len(node.PotentialFactories), node.PotentialFactories)
		}
	} else {
		t.Errorf("No node matching %v was found", typeB)
	}
}

func TestGetShouldPublishAfterInit(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	i, err := Get((*dependency)(nil))

	if err != nil {
		t.Fatal("Get returned error:", err)
	}

	if i == nil {
		t.Fatal("Get returned pointer to nil, this can never be set")
	}

	v, ok := i.(*dependency)

	if !ok {
		t.Fatalf("Get retunred value not castable to given type: %T", i)
	}

	if !isZero(*v) {
		t.Fatalf("Get returned non-zero value before init: %+v", *v)
	}

	dep := &dependency{"42"}

	MustRegister(dep)

	if v == nil {
		t.Fatalf("Wanted value was set to nil after register")
	}

	if !isZero(*v) {
		t.Fatalf("Wanted value is non-zero after register but before init: %+v", *v)
	}

	err = Init()

	if err != nil {
		t.Fatalf("Init failed: %v", err)
	}

	if isZero(*v) {
		t.Fatalf("Wanted value is stil zero-value after init: %+v", *v)
	}

	if v.Name != dep.Name {
		t.Fatalf("Got value that is not equal to injected dependecy:\nWanted: %#v\nGot: %#v", *dep, *v)
	}
}

func TestIsZero(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	if !isZero(nil) {
		t.Error("isZero returned false for nil")
	}

	var i interface{}
	if !isZero(i) {
		t.Error("isZero returned false for unintialised interface{}")
	}

	i = nil
	if !isZero(i) {
		t.Error("isZero returned false for nil interface{}")
	}

	var d *dependency
	if !isZero(d) {
		t.Error("isZero returned false for unintialised pointer to struct")
	}

	d = nil
	if !isZero(d) {
		t.Error("isZero returned false for nil pointer to struct")
	}

	i = d
	if !isZero(i) {
		t.Error("isZero returned false for interface{} type to nil pointer")
	}

	i = &d
	if isZero(i) {
		t.Errorf("isZero returned true for interface{} typeo to pointer of pointer to nil: %#v", i)
	}

	d = &dependency{}
	if isZero(d) {
		t.Errorf("isZero returned true for pointer to struct instance: %#v", d)
	}

	var s dependency
	if !isZero(s) {
		t.Errorf("isZero returned false for unintialised struct: %#v", s)
	}

	s = dependency{}
	if !isZero(s) {
		t.Errorf("isZero returned false for empty struct: %#v", s)
	}

	s = dependency{"hello world"}
	if isZero(s) {
		t.Errorf("isZero returned true for non-emtpy struct: %#v", s)
	}
}

func TestPrototypeBehaviourShouldReturnNewInstance(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	creater := &prototypeBehaviourCreater{}

	_, err := RegisterCreater(creater)

	if err != nil {
		t.Fatalf("Failed to register valid creater: %v", err)
	}

	MustInit()

	a := GetNow((*prototypeBehaviourCreater)(nil)).(*prototypeBehaviourCreater)

	if a == nil {
		t.Fatal("Container retunred nil after init")
	}

	a.num = 128

	b := GetNow((*prototypeBehaviourCreater)(nil)).(*prototypeBehaviourCreater)

	if b == nil {
		t.Fatal("Container retunred nil after init")
	}

	b.num = 256

	if a.num == b.num {
		t.Fatal("Container did not return a prototype instance. a.num was changed by changing b.name")
	}
}

func TestInitMustDetectCycles(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	MustRegister(&dependsOnSelf{})

	if err := defaultContainer.(*DefaultContainer).RebuildGraph(); err != nil {
		t.Fatalf("Could not build graph: %v", err)
	}

	err := defaultContainer.(*DefaultContainer).detectCycle()

	t.Log("Got error message:\n", err)

	if err == "" {
		t.Fatal("Init did not detect Creater, that has itself as dependecy as cycle")
	}
}

func TestGetWithInterfaceType(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	factory1 := func() fmt.Stringer {
		var tmp fmt.Stringer = &StringerImpl{}
		return tmp
	}

	MustRegister(factory1)

	fromContainer := MustGet(new(fmt.Stringer)).(*fmt.Stringer)

	MustInit()

	if fromContainer == nil {
		t.Fatalf("From container is nil after init")
	}
	s := (*fromContainer).String()

	if s != "Dummy stringer" {
		t.Fatalf("Registerd Stringer did not return 'Dummy stringer': %v", s)
	}

	resetAll()

	factory2 := func() *fmt.Stringer {
		var tmp fmt.Stringer = &StringerImpl{}
		return &tmp
	}

	MustRegister(factory2)

	fromContainer = MustGet((*fmt.Stringer)(nil)).(*fmt.Stringer)

	MustInit()

	if fromContainer == nil {
		t.Fatalf("From container is nil after init")
	}

	s = (*fromContainer).String()

	if s != "Dummy stringer" {
		t.Fatalf("Registerd Stringer did not return 'Dummy stringer': %v", s)
	}

}

func TestSingletonReturningSameValue(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	r := &singleTonTestStruct{
		num: 42,
	}

	MustRegister(r)

	MustInit()

	r2 := GetNow(r).(*singleTonTestStruct)

	r2.num = 0

	if r2.num != r.num {
		t.Fatalf("Container did not use given instnace as singleton! Value of original value did not change: registered: %p %+v, from container: %p %+v", r, *r, r2, r2)
	}
}

func TestRegisterShouldReturnErrorOnInactiveContainer(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	err := defaultContainer.Stop()

	if err != nil {
		t.Fatalf("Error of container.Stop was not nil: %v", err)
	}

	_, err = Register(&struct{}{})

	if err == nil {
		t.Error("Error was nil after calling Register with struct on inactive container")
	}

	mustRegisterMustPanic := func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				t.Error("MustRegister did not panic")
			}
		}()
		MustRegister(&struct{}{})
	}

	mustRegisterMustPanic(t)

	_, err = RegisterCreater(&oneDependency{})

	if err == nil {
		t.Error("Error was nil after calling RegisterCreater on inactive container")
	}

	mustRegisterCreaterMustPanic := func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				t.Error("MustRegisterCreater did not panic")
			}
		}()
		MustRegisterCreater(&oneDependency{})
	}

	mustRegisterCreaterMustPanic(t)

	_, err = RegisterInstance(&struct{}{})

	if err == nil {
		t.Error("Error was nil after calling RegisterInstance on inactive container")
	}

	mustRegisterInstanceMustPanic := func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				t.Error("MustRegisterInstance did not panic")
			}
		}()
		MustRegisterInstance(&struct{}{})
	}

	mustRegisterInstanceMustPanic(t)

	_, err = RegisterFunc(func() struct{} { return struct{}{} })

	if err == nil {
		t.Error("Error was nil after calling RegisterFunc on inactive container")
	}

	mustRegisterFuncMustPanic := func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				t.Error("MustRegisterInstance did not panic")
			}
		}()
		MustRegisterFunc(func() struct{} { return struct{}{} })
	}

	mustRegisterFuncMustPanic(t)

}

func TestRegisterFuncShouldReturnErrorWithInvalidInput(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	_, err := RegisterFunc(nil)

	if err == nil {
		t.Error("Error is nil after calling RegisterFunc with nil")
	}

	var f func() struct{}

	_, err = RegisterFunc(f)

	if err == nil {
		t.Error("Error is nil after calling RegisterFunc with uninitialized variable")
	}

	notAFunc := 123

	_, err = RegisterFunc(notAFunc)

	if err == nil {
		t.Error("Error is nil after calling RegisterFunc with a non-function parameter")
	}
}

func TestRegiserCreaterShouldReturnErrorWithInvalidInput(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	_, err := RegisterCreater(nil)

	if err == nil {
		t.Error("Error is nil after calling RegiserCreater with nil")
	}

	_, err = RegisterCreater(&createrReturningFalse{})

	if err == nil {
		t.Error("Error is nil after calling RegisterCreater with Creater.IsIoCCreater returning false")
	}

	_, err = RegisterCreater(&createrWithUndefinedBehaviour{})

	if err == nil {
		t.Error("Error is nil after calling RegisterCreater with Creater.GetIoCBeahaviour returning BUndefined")
	}

}

func TestRegiserInstanceShouldReturnErrorWithInvalidInput(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	_, err := RegisterInstance(nil)

	if err == nil {
		t.Error("Error is nil after calling RegisterInstance with nil")
	}

	var zero struct {
		name string
		num  int
	}

	_, err = RegisterInstance(zero)

	if err == nil {
		t.Error("Error is nil after calling RegisterInstance with a zero value")
	}

	_, err = RegisterInstance(&StringerImpl{})

	if err != nil {
		t.Errorf("Failed to prepare test case: %v", err)
	} else {
		_, err = RegisterInstance(&StringerImpl{})

		if err == nil {
			t.Error("Error is nil after calling RegisterIntance twice with same type")
		}
	}

}
