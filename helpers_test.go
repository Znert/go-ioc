package ioc

import (
	"os"
	"reflect"
	"testing"
)

func TestIsFragile(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	currVal := os.Getenv("GO_IOC_FRAGILE")

	os.Setenv("GO_IOC_FRAGILE", "true")

	if !isFragile() {
		t.Error("isFragile retunred false although GO_IOC_FRAGILE was set to 'true'")
	}

	os.Setenv("GO_IOC_FRAGILE", "")

	if isFragile() {
		t.Error("isFragile returned true altough GO_IOC_FRAGILE was set to an empty string")
	}

	os.Setenv("GO_IOC_FRAGILE", currVal)
}

func TestIsNil(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	if !isNil(nil) {
		t.Error("isNil retunred false altough nil was given")
	}

	elem1 := struct{}{}

	if isNil(elem1) {
		t.Error("isNil retunred true for struct value")
	}

	elem2 := &struct{}{}

	if isNil(elem2) {
		t.Error("isNil retunred true for non nil pointer value")
	}

	elem2 = nil

	if !isNil(elem2) {
		t.Error("isNil returned false for nil pointer to struct")
	}

	var elem3 interface{}

	if !isNil(elem3) {
		t.Error("isNil returned false for nil interface")
	}

	elem3 = 3

	if isNil(elem3) {
		t.Error("isNil returned true for non nil interface")
	}
}

func TestGetStructValue(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	value := reflect.ValueOf(&struct{}{})

	value, err := getStructValue(value)

	if err != nil {
		t.Fatal("Failed to convert pointer to struct to struct:", err)
	}

	if value.Kind() != reflect.Struct {
		t.Error("getStructValue returned non struct for pointer to struct: ", value.Kind())
	}

	orig := reflect.ValueOf(nil)
	value, err = getStructValue(orig)

	if err == nil {
		t.Fatal("getStructValue returned no error for invalid reflect.Value")
	}

	if orig != value {
		t.Error("getStructValue returned different value than was given:\ngiven:", orig, "\ngot:", value)
	}

	var myStruct *struct{}
	orig = reflect.ValueOf(myStruct)
	value, err = getStructValue(orig)

	if err == nil {
		t.Fatal("getStructValue returned no error for nil pointer")
	}

	if orig != value {
		t.Error("getStructValue returned different value than was given:\ngiven:", orig, "\ngot:", value)
	}
}

func TestGetPointerType(t *testing.T) {
	afterEach := beforeEach(t)
	defer afterEach(t)

	var instance interface{} = struct {
		name string
	}{
		"Herb",
	}

	ptr := &instance

	ty, err := getPointerType(reflect.TypeOf(&ptr))

	if err != nil {
		t.Fatal("Failed to convert **interface to *interface:", err)
	}

	if ty.Kind() != reflect.Ptr {
		t.Error("Kind of returned value is not ptr:", ty.Kind())
	} else if ty.Elem().Kind() != reflect.Interface {
		t.Error("Kind of returned child value is not interface:", ty.Elem().Kind())
	}

	ty, err = getPointerType(nil)

	if err == nil {
		t.Error("getPointerType did not return any error when nil was given")
	}

	if ty != nil {
		t.Error("Returned type is not nil when nil was given:", ty)
	}

	ty, err = getPointerType(reflect.TypeOf(struct{}{}))

	if err != nil {
		t.Fatal("Failed to convert struct to *struct:", err)
	}

	if ty.Kind() != reflect.Ptr {
		t.Error("Kind of returned value is not ptr:", ty.Kind())
	} else if ty.Elem().Kind() != reflect.Struct {
		t.Error("Kind of returned child value is not interface:", ty.Elem().Kind())
	}

}
