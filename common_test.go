package ioc

import "testing"

func beforeEach(t *testing.T) func(t *testing.T) {
	return func(t *testing.T) {
		testCounter++
		resetAll()
	}
}
