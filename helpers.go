package ioc

import (
	"fmt"
	"os"
	"reflect"
	"runtime"
)

// TODO: Document functions

func resetAll() {
	defaultContainer = NewContainer()
}

func isFragile() bool {
	return os.Getenv("GO_IOC_FRAGILE") != ""
}

func isZero(i interface{}) bool {
	if i == nil {
		return true
	}
	v := reflect.ValueOf(i)
	return i == nil || reflect.DeepEqual(i, reflect.Zero(v.Type()).Interface())
}

func isNil(i interface{}) bool {
	if i == nil {
		return true
	}
	return isNilVal(reflect.ValueOf(i))
}

func isNilVal(v reflect.Value) bool {
	return (isNilable(v.Kind()) && v.IsNil())
}

func isNilable(kind reflect.Kind) bool {
	return kind == reflect.Ptr || kind == reflect.Interface || kind == reflect.Slice ||
		kind == reflect.Map || kind == reflect.Chan || kind == reflect.Func
}

func getStructValue(t reflect.Value) (rt reflect.Value, e error) {
	defer func() {
		if r := recover(); r != nil {
			e = fmt.Errorf("Failed to convert %v to a struct value: %v", t.Type(), r)
			if isFragile() {
				panic(e)
			}
		}
	}()

	if t.Kind() == reflect.Invalid {
		return t, fmt.Errorf("Cannot convert invalid value to struct")
	}

	if isNilVal(t) {
		return t, fmt.Errorf("Cannot convert nil value to struct")
	}

	deref := t

	for deref.Kind() == reflect.Ptr {
		deref = deref.Elem()
	}

	return deref, nil
}

func getPointerType(t reflect.Type) (rt reflect.Type, e error) {
	if t == nil {
		err := fmt.Errorf("nil type given")
		if isFragile() {
			panic(err)
		}
		return nil, err
	}
	defer func() {
		if r := recover(); r != nil {
			rt = nil
			e = fmt.Errorf("Failed to convert %v to a single pointer type: %v", t, r)
			if isFragile() {
				panic(e)
			}
		}
	}()

	if t.Kind() == reflect.Ptr {
		// t is already a pointer
		// deference (if neccessary) until its pointed value is not a pointer
		deref := t

		for deref.Elem().Kind() == reflect.Ptr {
			deref = deref.Elem()
		}

		return deref, nil
	}

	// t is not a pointer, make a pointer to t
	return reflect.PtrTo(t), nil
}

func getNormalizedValue(wanted reflect.Type, got reflect.Value) (rt reflect.Value, e error) {

	if wanted == got.Type() {
		return got, nil
	}

	defer func() {
		if r := recover(); r != nil {
			rt = got
			e = fmt.Errorf("Failed to convert %v to %v: %v", got.Type(), wanted, r)
		}
	}()

	// TODO: Optimize: Currently we go down and up the pointer level

	structWanted := wanted
	wantedDepth := 0
	for structWanted.Kind() == reflect.Ptr {
		structWanted = structWanted.Elem()
		wantedDepth++
	}

	gotStruct := got
	gotDepth := 0
	for gotStruct.Kind() == reflect.Ptr {
		gotStruct = gotStruct.Elem()
		gotDepth++
	}

	if structWanted.Kind() == reflect.Interface {
		if !gotStruct.Type().AssignableTo(structWanted) {
			temp := gotStruct
			gotStruct = reflect.New(gotStruct.Type())
			gotStruct.Elem().Set(temp)
			gotDepth--

			if !gotStruct.Type().AssignableTo(structWanted) {
				return got, fmt.Errorf("Cannot convert %v to interface type %v", got.Type(), wanted)
			}
		}
	} else if gotStruct.Type() != structWanted {
		return got, fmt.Errorf("Cannot convert %v as %v", got.Type(), wanted)
	}

	for gotDepth < wantedDepth {
		temp := got
		got = reflect.New(got.Type())
		got.Elem().Set(temp)
		gotDepth++
	}

	for gotDepth > wantedDepth {
		got = got.Elem()
		gotDepth--
	}

	return got, nil

}

func publishToChannel(client *WaitingClient, node *DependencyNode) error {
	i, err := node.GetInstance()
	if err != nil {
		return fmt.Errorf("Failed to get instance: %v", err)
	}
	normalizedVal, err := getNormalizedValue(client.requestedType, reflect.ValueOf(i))
	if err != nil {
		return fmt.Errorf("Failed to normalize value: %v", err)
	}

	// NOTE: This code relies on go's deadlock detection to find any incorrectly behaving clients

	// Try to publish
	client.ch <- normalizedVal.Interface()

	// Publish successful, wait for completion of client
	<-client.ch

	// Client is done processing the instance
	return nil
}

func nameOf(v *reflect.Value) string {
	if v == nil {
		return "<nil>"
	}
	if v.Kind() == reflect.Func {
		if rf := runtime.FuncForPC(v.Pointer()); rf != nil {
			return rf.Name()
		}
	}
	return v.String()
}

type tarjanElem struct {
	node    *DependencyNode
	index   uint
	onstack bool
}

// detectCycle will detect any cyclic dependecies in the graph.
//
// Adaptation from Tarjan's strongly connected components algorithm
// https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
func detectCycle(stack map[*DependencyNode]tarjanElem, node *DependencyNode, index uint) (string, uint) {

	currElem := tarjanElem{
		node:    node,
		index:   index,
		onstack: true,
	}

	if len(node.Children) == 0 {
		return "", currElem.index
	}

	index++

	stack[node] = currElem

	var err string
	for _, child := range node.Children {
		if elem, ok := stack[child]; !ok {
			// Child has not been visited yet
			err, index = detectCycle(stack, child, index)
			if err != "" {
				// There is a loop found
				if index != 0 {
					// index is not yet 0
					// The root of the loop has not yet been visisted
					err = err + ", " + node.InstanceType.String()
				} else if index == currElem.index {
					// This is the root of the loop
					// Set
					index = 0
				}
				return err, index
			}
		} else if elem.onstack && elem.index <= currElem.index { // smaller or _equal_ because node could be its own parent
			// Elem is on stack and has a smaller index, loop detected
			err = "Loop detected! Involved nodes: " + child.InstanceType.String() + ", " + node.InstanceType.String()
			return err, elem.index
		}

		// Else, elem is visited, but not on stack. It was visisted on an earlier, successful traversal
	}

	currElem.onstack = false

	return "", index

}

func containsChild(slice []*DependencyNode, value *DependencyNode) bool {
	for _, val := range slice {
		if val == value {
			return true
		}
	}
	return false
}

func indexOfOutput(slice []*DependencyNode, value *DependencyNode) int {
	for i, val := range slice {
		if val == value {
			return i
		}
	}
	return -1
}
