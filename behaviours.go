package ioc

import (
	"fmt"
	"reflect"
)

// Behaviour defines how and when a factory creates an instance.
type Behaviour uint

const (
	// BUndefined is used when a node does not specify its behaviour.
	BUndefined Behaviour = iota
	// BDefault marks nodes, that do not have a behaviour specified and the Container's default behaviour should be used.
	BDefault
	// BSingleton means, that an instance is only created once and reused from that point on.
	BSingleton
	// BPrototype means, that every time the instance is retrieved, the original instance will be shallow cloned.
	BPrototype
	// BDeepPrototype is the same as Prototype, but it will try a deep clone instead.
	BDeepPrototype
	// BUncached values will be created every time it is retrieved.
	// This can lead to a performance penalty depending on the factory that creates it (especially for multi value
	// factories).
	BUncached
	// This value is a guard for the compiler to check if all arrays have the corret length
	// It MUST be the last value in this constance list!
	bHighesValue
)

var behaviourNames = [bHighesValue]string{
	"undefined",
	"default",
	"singleton",
	"prototype",
	"deep-prototype",
	"uncached",
}

var behaviourGetters [bHighesValue]func(*DependencyNode) (interface{}, error)

func init() {
	behaviourGetters = [...]func(*DependencyNode) (interface{}, error){
		func(node *DependencyNode) (interface{}, error) {
			// Get() for BUndefined
			return nil, fmt.Errorf("Behaviour for node %v is %s", node.InstanceType, BUndefined)
		},
		func(node *DependencyNode) (interface{}, error) {
			// Get() for BDefault
			return nil, fmt.Errorf("Behaviour for node %v is %s", node.InstanceType, BDefault)
		},
		func(node *DependencyNode) (interface{}, error) {
			// Get() for BSingleton
			return node.Instance, nil
		},
		func(node *DependencyNode) (interface{}, error) {
			// Get() for BPrototype
			val, err := getStructValue(reflect.ValueOf(node.Instance))
			if err != nil {
				err = fmt.Errorf("Cannot convert %v to struct type: %v", node.InstanceType, err)
				if isFragile() {
					panic(err)
				}
				return nil, err
			}
			if node.InstanceType.Kind() == reflect.Struct {
				return val.Interface(), nil
			}
			val, err = getNormalizedValue(node.InstanceType, val)
			if err != nil {
				err = fmt.Errorf("Cannot convert struct type of %v to type %v: %v", node.InstanceType, node.InstanceType, err)
				if isFragile() {
					panic(err)
				}
				return nil, err
			}
			return val.Interface(), nil
		},
		func(node *DependencyNode) (interface{}, error) {
			// Get() for BDeepPrototype
			return nil, fmt.Errorf("Function not yet implemented")
		},
		func(node *DependencyNode) (interface{}, error) {
			// Get() for BUncached
			if node.Type != NFactory {
				return nil, fmt.Errorf("Behaviour of %v node is %s, but is of type %s", node.InstanceType, BUncached, node.Type)
			}
			if node.Factory == nil {
				return nil, fmt.Errorf("Cannot call factory of node %v: Factory is nil", node.InstanceType)
			}
			_, err := node.Factory.Call()

			if err != nil {
				return nil, fmt.Errorf("Failed at calling factory: %v", err)
			}

			return node.Factory.GetCachedFor(node)
		},
	}
}

func (b Behaviour) String() string {
	return behaviourNames[b]
}

// GetInstance returns an instance from the node based on the Behaviour.
//
// The node must be valid.
func (b Behaviour) GetInstance(node *DependencyNode) (interface{}, error) {
	if node == nil {
		return nil, fmt.Errorf("nil given")
	}
	return behaviourGetters[b](node)
}
