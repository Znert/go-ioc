package main

import (
	"fmt"

	ioc "gitlab.com/Znert/go-ioc"
)

// ServiceA provides us with a name
type ServiceA struct {
	Name string
}

// ServiceB requires a ServiceA
type ServiceB struct {
	ServiceA *ServiceA
	MagicNum int
}

// Factory will take a ServiceA and use it to create a ServiceB
func Factory(a *ServiceA) *ServiceB {
	return &ServiceB{a, 42}
}

// We reigster the factory
var _ = ioc.MustRegister(Factory)

// ..and an instance of ServiceA
// Sidenote: Both use MustRegister, ioc will figure out how to handle a func vs an instance
var _ = ioc.MustRegister(&ServiceA{"Mailüfterl"})

// Now we want to get ServiceB
var serviceB = ioc.MustGet((*ServiceB)(nil)).(*ServiceB)

func main() {

	// The main thread calls the Init function
	ioc.MustInit()

	// ServiceB is now not zero and can be used
	fmt.Println("serviceB's magic number:", serviceB.MagicNum)
	// Also ServiceA got injected
	fmt.Println("serviceA's name:", serviceB.ServiceA.Name)
}
