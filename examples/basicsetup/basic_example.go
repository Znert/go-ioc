package main

import (
	"fmt"

	ioc "gitlab.com/Znert/go-ioc"
)

// MyService is a sample service.
// We want the container to manage an instance of this service.
type MyService struct {
	name string
}

// SayHello will print out a Hello World message.
func (m *MyService) SayHello() {
	fmt.Println("Hello World! May name is", m.name)
}

// Some file (the provider) must register the service to the IoC container
var myService = ioc.MustRegister(&MyService{
	name: "Ada Lovelace",
}).(*MyService)

// Some consumer wants to have the service as global variable.
//
// Give a pointer value to the type you want to get.
var fromContainer = ioc.MustGet((*MyService)(nil)).(*MyService)

func main() {

	// The main thread calls the Init function
	ioc.MustInit()

	// fromContainer is now the registerd instance
	fromContainer.SayHello()

	// Output: Hello World! May name is Ada Lovelace
}
