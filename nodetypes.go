package ioc

import (
	"fmt"
	"reflect"
)

// nodeType defines what kind of node a DependencyNode is.
//
// Currently, there are two valid types (depending on what was given when creating the node):
//   * Instance
//   * Factory method
//
// There is also one invalid type to specify, that a node was not yet set.
type nodeType uint

const (
	// NUndefined means that the type of node is not specified (usally if the node has just been created or is a dummy node)
	NUndefined nodeType = iota
	// NInstance is used for nodes that have an instance given.
	NInstance
	// NFactory shows that a node has a Factory to produce instances
	NFactory
	// NInitializer is used for nodes, that do no represent types per se, but raterh a factory that does not produces
	// any types but consumes some. Those factories are generally used to initialize some other, internal services.
	//
	// Nodes having this type may have not nodeType set.
	NInitializer
	// This value is a guard for the compiler to check if all arrays have the corret length
	// It MUST be the last value in this constance list!
	nHighesValue
)

var typeNames = [nHighesValue]string{
	"undefined",
	"instance",
	"factory",
	"initializer",
}

var typeInitializer [nHighesValue]func(*DependencyNode) error

func init() {
	typeInitializer = [...]func(*DependencyNode) error{

		func(node *DependencyNode) error {
			// InitNode() for NUndefined
			err := fmt.Errorf("Node type is '%s'", NUndefined)
			if isFragile() {
				panic(err)
			}
			return err
		},
		func(node *DependencyNode) error {
			// InitNode() for NInstance
			// Nothing to do
			node.IsInit = true
			return nil
		},
		func(node *DependencyNode) (e error) {
			// InitNode() for NFactory
			defer func() {
				if e == nil {
					node.IsInit = true
				}
			}()

			if node.Factory == nil {
				_, err := node.CallFactory()
				if err != nil {
					return fmt.Errorf("Failed at calling factory %v: %v", node.Factory, err)
				}
			}

			result, err := node.Factory.GetCachedFor(node)

			if err != nil {
				return err
			}

			var val reflect.Value
			val, err = getNormalizedValue(node.InstanceType, *result)

			if err != nil {
				return fmt.Errorf("Failed to cast '%v' (returned by factory) to '%v' (registered type): %v", result.Type(), node.InstanceType, err)
			}

			node.Instance = val.Interface()
			node.InstanceType = node.Factory.CreatesTypes[node.FactoryIndex]

			return nil
		},
		func(node *DependencyNode) (e error) {
			// InitNode for NIniter
			defer func() {
				if e == nil {
					node.IsInit = true
				}
			}()

			_, err := node.CallFactory()
			if err != nil {
				return fmt.Errorf("Failed at calling factory %v: %v", node.Factory, err)
			}

			return nil
		},
	}
}

func (t nodeType) String() string {
	return typeNames[t]
}

// InitNode will initialise the given node dependant on the type.
//
// The node must be valid.
func (t nodeType) InitNode(node *DependencyNode) error {
	return typeInitializer[t](node)
}
