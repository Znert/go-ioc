package ioc

import (
	"fmt"
	"reflect"
)

// FactoryType describes what kind a factory is
type FactoryType uint

const (
	// FUndefined is used if the type of Factory cannot be determined.
	// It usally marks an error state of the factory.
	FUndefined FactoryType = iota
	// FCreater is used for factories which have a Creater instance to call.
	FCreater
	// FMethod says that the factory does not have an instance, but a function to use.
	FMethod
)

var factoryTypeNames = []string{
	"undefined",
	"creater",
	"method",
}

func (t FactoryType) String() string {
	if t < FactoryType(len(factoryTypeNames)) {
		return factoryTypeNames[t]
	}
	if isFragile() {
		panic(fmt.Errorf("Unknown factory type found: %d", t))
	}
	return "[unknown factory type]"
}

// Factory is the inner representation of a Creater.
//
// It provides more meta information for easier and faster handling of dependencies.
type Factory struct {
	// Reference to the Creater
	Creater Creater
	// Reference to the Create method
	Method *reflect.Value
	// Slice of Types that are needed for this factory as input parameter for Create
	InputTypes []reflect.Type
	// Slice of dependencyNodes whose instances are required as input parameter for Create
	Input []*DependencyNode
	// Slice of Types that are created by this factory
	CreatesTypes []reflect.Type
	// Slice of dependencyNodes that are created by this factory
	Creates []*DependencyNode
	// List of context in which this Creater can be used. If empty the Creater is always active, unless there is a
	// fitting other Factory for the active context(s).
	Contexts []string
	// Defines the type this Factory represents
	Type FactoryType
	// Behaviour of this factory
	Behaviour Behaviour
	// ResultCache saves the last result of Call()
	ResultCache []reflect.Value
}

// NewFactory returns a new factory filled sane defaults.
// However, this factory is not yet valid and has to be updated by the caller.
func NewFactory() *Factory {
	return &Factory{
		nil,                 // Creater
		nil,                 // Method
		[]reflect.Type{},    // InputTypes
		[]*DependencyNode{}, // Input
		[]reflect.Type{},    // CreatesTypes
		[]*DependencyNode{}, // Creates
		[]string{},          // Contexts
		FUndefined,          // Type
		BUndefined,          // Behaviour
		nil,                 //ResultCache
	}
}

// GetType returns the kind of Factory this is.
func (f *Factory) GetType() FactoryType {
	if f == nil {
		return FUndefined
	}
	if f.Creater == nil {
		return FMethod
	}
	return FCreater
}

// Call will execute the factory method of this factory and return the result.
//
// If it fails, error will be set and nil will be returned.
func (f *Factory) Call() (result []reflect.Value, e error) {

	if f.Method == nil {
		err := fmt.Errorf("Factory method is nil")
		if isFragile() {
			panic(err)
		}
		return nil, err
	}

	defer func() {
		if r := recover(); r != nil {
			e = fmt.Errorf("Failed to call factory: %v", r)
			if isFragile() {
				panic(e)
			}
			result = nil
		}
	}()

	in := make([]reflect.Value, len(f.Input))
	for i, v := range f.Input {
		var instance interface{}
		var err error
		if instance, err = v.GetInstance(); err != nil {
			return nil, fmt.Errorf("Failed to call factory: Failed to initialise node %v: %v", v.InstanceType, err)
		}
		in[i] = reflect.ValueOf(instance)
	}

	returned := f.Method.Call(in)
	f.ResultCache = returned
	return returned, nil
}

// GetCachedFor returns for a given node the corresponding Value from the called factory.
//
// If the factory has never been called before, it will be called and a newly returned value
// will be returned.
//
// If given node is not produced by this Factory, or any other error occures while getting
// the value, nil and an error is returned.
// Otherwise a non-nil value and no error is returned. This returned value is not yet
// normalized and its up to the client to cast the value to its needs.
func (f *Factory) GetCachedFor(node *DependencyNode) (*reflect.Value, error) {
	if node == nil {
		return nil, fmt.Errorf("Cannot get cached value: nil given")
	}

	if f.ResultCache == nil {
		if _, err := f.Call(); err != nil {
			return nil, err
		}
	}

	if node.FactoryIndex < 0 || node.FactoryIndex >= len(f.ResultCache) {
		return nil, fmt.Errorf("Node %v is not produced by factory %s: Got index: %d, number of output paramters: %d", node.InstanceType, f, node.FactoryIndex, len(f.ResultCache))
	}

	result := f.ResultCache[node.FactoryIndex]

	nodePtr, err := getPointerType(node.InstanceType)
	if err != nil {
		return nil, fmt.Errorf("Failed to get key for %v: %v", node.InstanceType, err)
	}
	resultPtr, err := getPointerType(result.Type())
	if err != nil {
		return nil, fmt.Errorf("Failed to get key for %v: %v", result.Type(), err)
	}

	if nodePtr != resultPtr {
		return nil, fmt.Errorf("Node %v is not produced by this factory %s", node.InstanceType, f)
	}

	return &result, nil
}

func (f *Factory) String() string {
	if f == nil || f.Method == nil {
		return "<nil>"
	}
	return f.Method.Type().String()
}
