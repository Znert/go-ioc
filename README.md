# Go IoC

This project attempts to implement inversion of control with go.

## Goals
### Easy to Use

In a lot of cases you just want to add and get instances of a specific type.
Why make it complecated? Who cares how the backend is configured excatly!
This implemtation hopes to abstract from all this noise to get you going
quickly.

See for yourself via the examples in the go doc!

### Configurable
  
So you've done your simple stuff and got an application up and running. But
now you need to change the behaviour of your app and get more specific. No
problem! You can easily change the behaviour of a container without having
to change your whole code base.

### Configure in code, not in file

Extending to the point above, configuration should always be visible to the
programmer, configs often hinder this: They are out of place and not visibly
connected to any other code. And - as go currently does not support packaging
(very well) - this also means that you have to ship your program with the
config.

So this tool will use in-code configuration over external text files.

### Lightweight Code

Every coded a small project and the compiled code just exploded in size? 
This won't happen here! The code is as simple and small as possible which
reduces not only complexity of the backend, it also improves performance.

### Robust

This library should not change over time, only extend. This means that you can
update this lib without having to update your code. 

### Open Source

Of course this project will always be free to use and public. 

### Well Supported

Have a problem? Post an issue to this git!

### Free Cookies

Who doesn't like cookies?

## How's Progress?

This project is currently in its planning phase. Also, as this is a small, 
one man project which is only worked on in the free time, progress might be 
slow, especially at the beginning.

### What is currently worked on:
* Getting closer to first public aplpha release