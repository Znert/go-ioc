package ioc

import (
	"fmt"
	"reflect"
)

// DefaultContainer is used to collect instances into a pool. It is the default implementation of the Container
// interface.
//
// Contexts are not thread save. Meaining: It could be possible that factories are added while others still get nil.
// To avoid this, it is recommended to add all dependencies via global variables and init the context as soon as
// possible in the main thread.
type DefaultContainer struct {
	Nodes            map[reflect.Type]*DependencyNode // Map of all nodes in this container
	RootNodes        map[reflect.Type]*DependencyNode // Map of all nodes that do not have any parents
	IniterNodes      []*DependencyNode                // A slice of nodes that have type NInitializer. Not a map as they do not have a key.
	isInit           bool                             // True if init has been called for this context
	isActive         bool                             // True if instances can be retrieved from this container
	isLazy           bool                             // True if instances may have to be instanciated upon retrieving
	DefaultBehaviour Behaviour                        // The behaviour to use for all factories that do not specify a behaviour.
	// List of active contexts. A Creater will be used, if at least one Context matches at least one active context or
	// the context array is empty.
	//
	// If no context is set (the array is empty), Factories with no set context will be used in favour of Factories with
	// context(s).
	//
	// Context can only be changed while IsInit is false. Otherwise changes are ignored.
	ActiveContexts []string
}

// Register is a helper method to register any kind of object.
//
// If any is a function, it is equivalent to RegisterFunc(any).
//
// If any is a Creater implementation, it is equivalent to RegisterCreater(any).
//
// And if any matches none of the above, it is equivalent to RegisterInstance(any).
func (cont *DefaultContainer) Register(any interface{}) (interface{}, error) {
	if !cont.IsActive() {
		return any, fmt.Errorf("Cannot register %T: Container is not active", any)
	}

	if isNil(any) {
		return any, fmt.Errorf("Cannot register %T: Nil is given", any)
	}

	switch { // Switch for non-type values
	case reflect.TypeOf(any).Kind() == reflect.Func:
		return cont.RegisterFunc(any)
	default:
		switch t := any.(type) { // Switch for different types
		case Creater:
			return cont.RegisterCreater(t)
		default:
			return cont.RegisterInstance(t)
		}
	}
}

// MustRegister does the same as Register, but panics if an error occures.
func (cont *DefaultContainer) MustRegister(any interface{}) interface{} {
	ret, err := cont.Register(any)

	if err != nil {
		panic(err)
	}

	return ret
}

// RegisterCreater will use given Creater implementation as a factory for a node.
//
// This method will return an non-nil error in the following cases:
//   * The container is not active anymore
//   * c is nil
//   * c.IsIoCCreater returns false (this is a save guard agains "accidental" implementations of the interface)
//   * c has no method named "IoCCreate"
//   * c.IoCCreate has multiple output values with the same underlying type (e.g. *MyType and MyType are regarded
//   	 the same)
//   * c.GetIoCBehaviour returns BUndefined
//   * At least one output type was previously registerd with an instance
//   * There was an error while parsing the input/output types of the IoCCreater function
func (cont *DefaultContainer) RegisterCreater(c Creater) (Creater, error) {
	if !cont.IsActive() {
		return c, fmt.Errorf("Cannot register %T: Container is not active", c)
	}

	if isNil(c) {
		// We do not allow nil, because one maybe implement the interface using `func (t MyCreater) IoCFunc()` instead of
		// using a (generally better) pointer receiver `func (t *MyCreater) IoCFunc()`
		return c, fmt.Errorf("Cannot register %T: Nil given", c)
	}

	if !c.IsIoCCreater() {
		return c, fmt.Errorf("Cannot register %T: %T implements Creater interface, but IsIOCCreater returned false", c, c)
	}

	cv := reflect.ValueOf(c)

	method := cv.MethodByName(CreaterMethodName)

	if isZero(method) {
		return c, fmt.Errorf("Cannot register %T: %T does not have a method named '%s' ", c, c, CreaterMethodName)
	}

	// Only ask creater once for Behaviour (to avoid change at runtime)
	b := c.GetIoCBehaviour()

	if b == BUndefined {
		return c, fmt.Errorf("Cannot register %T: Behaviour must not be undefined", c)
	}

	// Clone the contexts so that they cannot be tampered with later on
	origContexts := c.GetIoCContexts()
	clonedContexts := make([]string, len(origContexts))
	copy(clonedContexts, origContexts)

	// Create new factory instance used by all produced nodes
	factory := NewFactory()
	factory.Creater = c
	factory.Behaviour = b
	factory.Type = FCreater
	factory.Contexts = clonedContexts

	err := cont.registerFactoryNode(&method, factory)

	if err != nil {
		return c, fmt.Errorf("Cannot register %T: %v", c, err)
	}
	return c, nil
}

// MustRegisterCreater does the same as RegisterCreater but panics if it fails.
func (cont *DefaultContainer) MustRegisterCreater(c Creater) Creater {
	ret, err := cont.RegisterCreater(c)

	if err != nil {
		panic(err)
	}

	return ret
}

// RegisterInstance will register given instance to the IoC container. This instance will be handled like a singleton.
//
// This method will return an non-nil error in the following cases:
//   * i is a zero value
//   * Another node of i has already been registered.
//   * An error occured while parsing the instance type.
func (cont *DefaultContainer) RegisterInstance(i interface{}) (interface{}, error) {
	if !cont.IsActive() {
		return i, fmt.Errorf("Cannot register %T: Container is not active", i)
	}

	if isZero(i) {
		return i, fmt.Errorf("Cannot register %T: Zero value instance given", i)
	}

	iType := reflect.TypeOf(i)

	key, err := getPointerType(iType)
	if err != nil {
		return i, fmt.Errorf("Cannot register %v: %v", iType, err)
	}

	node := cont.Nodes[key]
	if node == nil {
		node = NewDepNode()
		cont.Nodes[key] = node
	} else if !node.IsDummy {
		return i, fmt.Errorf("Cannot register %T: Already registered", i)
	}

	// This node is a fully qualified node
	node.IsDummy = false

	// (Re-)set all values
	node.Factory = nil
	node.Behaviour = BDefault
	node.Instance = i
	node.InstanceType = iType
	node.Parents = []*DependencyNode{}
	node.Type = NInstance
	node.Behaviour = BSingleton

	// Do not override children

	// Add node to root nodes
	cont.RootNodes[key] = node

	return i, nil
}

// MustRegisterInstance does the same as RegisterInstance but panics if it fails.
func (cont *DefaultContainer) MustRegisterInstance(i interface{}) interface{} {
	ret, err := cont.RegisterInstance(i)

	if err != nil {
		panic(err)
	}

	return ret
}

// RegisterFunc will use given func as a factory for a node.
//
// This method will return an non-nil error in the following cases:
//   * The container is not active anymore
//   * f is nil
//   * f has multiple output values with the same underlying type (e.g. *MyType and MyType are regarded the same)
//   * At least one output type was previously registerd with an instance
//   * There was an error while parsing the input/output types of the IoCCreater function
func (cont *DefaultContainer) RegisterFunc(f interface{}) (interface{}, error) {

	if !cont.IsActive() {
		return f, fmt.Errorf("Cannot register %T: Container is not active", f)
	}

	if isNil(f) {
		return f, fmt.Errorf("Cannot register %T: Nil instance given", f)
	}

	v := reflect.ValueOf(f)

	if v.Kind() != reflect.Func {
		return f, fmt.Errorf("Cannot register %T: %T is not of kind '%v'", f, f, reflect.Func)
	}

	// Create a new factory of type FMethod with container default behaviour
	factory := NewFactory()
	factory.Type = FMethod
	factory.Behaviour = BDefault
	factory.Method = &v

	err := cont.registerFactoryNode(&v, factory)

	if err != nil {
		return f, fmt.Errorf("Cannot register %T: %v", f, err)
	}

	return f, nil
}

// MustRegisterFunc does the same as RegiserFunc, but panics if it fails.
func (cont *DefaultContainer) MustRegisterFunc(f interface{}) interface{} {
	ret, err := cont.RegisterFunc(f)

	if err != nil {
		panic(err)
	}

	return ret
}

// GetChannel returns a channel that will fire at most once with an instance that
// matches given interface.
//
// If getting fails, it will publish a zero instance to the channel. This, however,
// may also be the case if a zero instance was published to the container.
//
// If the node is not ready yet, the returned channel will be filled once the
// wanted instance is ready. But this function does not guarantee that this will ever
// happen.
//
// The consumer, once it received the instance and did all it needed to do,
// must then publish any value back to the channel. This tells the container
// that the cosumer finished and can therefore proceed.
//
// This pattern is neccessary to ensure that once the container initialized successfully,
// all instances are handled and can be used.
func (cont *DefaultContainer) GetChannel(i interface{}) chan interface{} {
	iType := reflect.TypeOf(i)

	key, err := getPointerType(iType)
	if err != nil {
		if isFragile() {
			panic(fmt.Errorf("Cannot get %v: Failed to convert %v to single pointer type: %v", iType, iType, err))
		}
		// Failed to convert, return zero value
		c := make(chan interface{}, 1)
		c <- reflect.Zero(iType).Interface()
		close(c)
		return c
	}

	node := cont.Nodes[key]

	if node == nil {
		node = NewDepNode()
		node.IsDummy = true
		node.InstanceType = iType
		cont.Nodes[key] = node
	}

	if !cont.IsInit() || node.IsDummy {
		// Either node or container is not ready yet, wait
		c := make(chan interface{}, 0)

		node.WaitingClients = append(node.WaitingClients, &WaitingClient{
			c,
			iType,
		})
		return c
	}

	// Container is init and node is not dummy

	// Node may not be init
	// This implementation will attempt to call init regardless of laziness of container

	if err = node.Init(); err != nil {
		// Failed to init node, return zero value
		c := make(chan interface{}, 1)
		c <- reflect.Zero(iType).Interface()
		return c
	}

	// Node is init, return immediately

	c := make(chan interface{}, 1)
	i, err = node.GetInstance()
	if err != nil {
		// Gettings instance failed
		if isFragile() {
			panic(fmt.Errorf("Failed to get channel for %v: %v", iType, err))
		}
		c <- reflect.Zero(iType).Interface()
		return c
	}
	var wanted reflect.Value
	wanted, err = getNormalizedValue(iType, reflect.ValueOf(i))
	if err != nil {
		// Conversion failed
		if isFragile() {
			panic(fmt.Errorf("Failed to get channel for %v: %v", iType, err))
		}
		c <- reflect.Zero(iType).Interface()
		return c
	}
	c <- wanted.Interface()
	return c
}

// Get returns an instance, that will later be set to the value from the container.
//
// This method will call GetChannel and handle syncronization with the container.
//
// The given instance must be a pointer value to the wanted struct or an interface type.
func (cont *DefaultContainer) Get(i interface{}) (interface{}, error) {
	if i == nil {
		return i, fmt.Errorf("nil given (this can also occure if nil was cast to any interface instead of *interface)")
	}

	vType := reflect.TypeOf(i)

	if vType.Kind() == reflect.Interface {
		vType = vType.Elem()
	} else if vType.Kind() != reflect.Ptr {
		return i, fmt.Errorf("Cannot get %v: Cannot call Get with non pointer type %v", vType, vType)
	}

	ch := cont.GetChannel(i)

	ret := reflect.New(vType.Elem())
	go func() {
		realDeal := reflect.ValueOf(<-ch)
		ret.Elem().Set(realDeal.Elem())
		ch <- nil
	}()
	return ret.Interface(), nil
}

// MustGetChannel returns a channel that will fire at most once with an instance that
// matches given interface. However, if it fails it will panic.
//
// In particular, this method - in contrast to GetChannel - will also fail if a non-lazy
// container is already initialized, but the value cannot be published immediately.
//
// If this method is called before [Must]Init of an non-lazy container is executed and
// the type cannot be resolved at initialization, and error will be produced.
//
// The consumer, once it received the instance and did all it needed to do,
// must then publish any value back to the channel. This tells the container
// that the cosumer finished and can therefore proceed.
//
// This pattern is neccessary to ensure that once the container initialized successfully,
// all instances are handled and can be used.
func (cont *DefaultContainer) MustGetChannel(i interface{}) chan interface{} {

	iType := reflect.TypeOf(i)

	key, err := getPointerType(iType)
	if err != nil {
		panic(fmt.Errorf("Cannot get %v: %v", iType, err))
	}

	node := cont.Nodes[key]

	if node == nil {
		if cont.IsInit() && !cont.IsLazy() {
			panic(fmt.Errorf("Cannot get %v: Node not found", iType))
		}
		node = NewDepNode()
		node.IsDummy = true
		node.InstanceType = iType
		cont.Nodes[key] = node
	}

	if !cont.IsInit() {
		// Either node or container is not ready yet, wait
		c := make(chan interface{}, 0)

		node.MustWaitingClients = append(node.WaitingClients, &WaitingClient{
			c,
			iType,
		})
		return c
	}

	// Container is init and node is not dummy

	if !node.IsInit {
		// Node is not init
		if cont.IsLazy() {
			// ...but container is lazy, call node.Init
			if err = node.Init(); err != nil {
				panic(fmt.Errorf("Cannot get %v: %v", iType, err))
			}
		} else {
			panic(fmt.Errorf("Cannot get %v: Container is initialized, but node %v is not", iType, node.InstanceType))
		}
	}

	// Node is init, return immediately

	c := make(chan interface{}, 1)
	i, err = node.GetInstance()
	if err != nil {
		// Gettings instance failed
		panic(fmt.Errorf("Failed to get channel for %v: %v", iType, err))
	}
	var wanted reflect.Value
	wanted, err = getNormalizedValue(iType, reflect.ValueOf(i))
	if err != nil {
		// Conversion failed
		panic(fmt.Errorf("Failed to get channel for %v: %v", iType, err))
	}
	c <- wanted.Interface()
	return c
}

// MustGet does the same as Get, but uses MustGetChannel instead of GetChannel.
func (cont *DefaultContainer) MustGet(i interface{}) interface{} {

	if i == nil {
		panic("nil given (this can also occure if nil was cast to an 'interface' instead of '*interface')")
	}

	vType := reflect.TypeOf(i)

	if vType.Kind() != reflect.Ptr {
		panic(fmt.Errorf("Cannot get %v: Cannot call GetInstance with non pointer type %v", vType, vType))
	}

	ch := cont.MustGetChannel(i)

	ret := reflect.New(vType.Elem())
	go func() {
		realDeal := reflect.ValueOf(<-ch)
		// TODO: Remove normalization, is guarantted by getchannel
		// Also remvoe in Get()
		wanted, err := getNormalizedValue(vType, realDeal)
		if err != nil {
			panic(fmt.Errorf("Cannot get %v: Failed convertion from %v to %v: %v",
				vType, realDeal.Type(), vType, err))
		}
		ret.Elem().Set(wanted.Elem())
		ch <- nil
	}()
	return ret.Interface()
}

// GetNow will return a value for given type immediately.
//
// If it fails, a nil value will be returned instead.
func (cont *DefaultContainer) GetNow(i interface{}) interface{} {
	iType := reflect.TypeOf(i)

	key, err := getPointerType(iType)
	if err != nil {
		if isFragile() {
			panic(fmt.Errorf("Cannot get %v: Failed to convert %v to single pointer type: %v", iType, iType, err))
		}
		// Failed to convert, return zero value
		return reflect.Zero(iType).Interface()
	}

	node := cont.Nodes[key]

	if node == nil {
		return reflect.Zero(iType).Interface()
	}

	inst, err := node.GetInstance()

	if err != nil {
		fmt.Printf("Got error: %v\n", err)
		return reflect.Zero(iType).Interface()
	}

	wanted, err := getNormalizedValue(iType, reflect.ValueOf(inst))

	if err != nil {
		if isFragile() {
			panic(fmt.Errorf("Cannot get %v: Failed to convert %v to %v", iType, node.InstanceType, iType))
		}
	}

	return wanted.Interface()
}

// Init will initialize all nodes and resolves all dependencies.
//
// If Init fails, it will return an error. In that case, no instances are published
// to any client.
//
// When all nodes are initialised, Init will publish the values to all waiting consumers.
func (cont *DefaultContainer) Init() (e error) {
	defer func() {
		if r := recover(); r != nil {
			err := fmt.Errorf("Cannot initialize: Panic while initialisation: %v", r)
			if isFragile() {
				panic(err)
			}
			e = err
		}

		if e != nil {
			for _, node := range cont.Nodes {
				node.Reset()
			}
		}
	}()

	// Build the Graph
	if err := cont.RebuildGraph(); err != nil {
		return fmt.Errorf("Cannot initialize: %v", err)
	}

	// Detect any cyclic dependencies
	if err := cont.detectCycle(); err != "" {
		return fmt.Errorf("Cannot initialize: %v", err)
	}

	// Init all nodes
	for _, node := range cont.Nodes {
		if err := node.Init(); err != nil {
			return fmt.Errorf("Cannot initialize: %v", err)
		}
	}
	for _, node := range cont.IniterNodes {
		if err := node.Init(); err != nil {
			return fmt.Errorf("Cannot initialize: %v", err)
		}
	}

	for _, node := range cont.Nodes {
		if node.IsDummy {
			if len(node.MustWaitingClients) > 0 {
				return fmt.Errorf("Cannot initialize: Mandatory instance of '%v' wanted, but none found", node.InstanceType)
			}
			node.Instance = reflect.Zero(node.InstanceType).Interface()
		} else if !node.IsInit {
			return fmt.Errorf("Cannot initialize: There are open dependencies for %v", node.InstanceType)
		}
	}

	// Init was successful, publish instances
	for _, node := range cont.Nodes {
		if err := node.Publish(); err != nil {
			return fmt.Errorf("Cannot initialize: %v", err)
		}
	}

	cont.isInit = true
	return nil
}

// MustInit does the same as Init, but panics if it fails
func (cont *DefaultContainer) MustInit() {
	err := Init()
	if err != nil {
		panic(err)
	}
}

// Stop will kill the Container.
func (cont *DefaultContainer) Stop() error {
	cont.isActive = false
	return nil
}

// Reset will set all values of this Container to its initial values.
//
// The Container will therefore also be cleaned from any registered
// Types.
//
// This is the only way to get a stoped context back running.
func (cont *DefaultContainer) Reset() error {
	cont.Nodes = make(map[reflect.Type]*DependencyNode)
	cont.RootNodes = make(map[reflect.Type]*DependencyNode)
	cont.isInit = false
	cont.isActive = true
	cont.isLazy = defaultIsLazy
	cont.DefaultBehaviour = defaultBehaviour
	cont.ActiveContexts = []string{}
	return nil
}

// IsLazy returns if this container is lazy loading instances.
func (cont *DefaultContainer) IsLazy() bool {
	return cont.isLazy
}

// SetLazy sets if this container is lazy loading values.
func (cont *DefaultContainer) SetLazy(lazy bool) {
	cont.isLazy = lazy
}

// IsActive returns if this container is currently active or not.
func (cont *DefaultContainer) IsActive() bool {
	return cont.isActive
}

// IsInit returns if this container is already initialized or not.
func (cont *DefaultContainer) IsInit() bool {
	return cont.isInit
}

// GetDefaultBehaviour returns the default Behaviour this container applies to
// Creaters, which specify the BDefault behaviour, or registered func and
// instances.
//
// If the default Behaviour is of type BUncached, instances will fall back to
// BDeepPrototype.
func (cont *DefaultContainer) GetDefaultBehaviour() Behaviour {
	return cont.DefaultBehaviour
}

// SetDefaultBehaviour sets the Behaviour the container applies to applies to
// Creaters, which specify the BDefault behaviour, or registered func and
// instances.
//
// If the default Behaviour is of type BUncached, instances will fall back to
// BDeepPrototype.
func (cont *DefaultContainer) SetDefaultBehaviour(b Behaviour) {
	cont.DefaultBehaviour = b
}

// GetActiveContexts returns the currently active contexts.
//
// The returned slice is a copy.
func (cont *DefaultContainer) GetActiveContexts() []string {
	clone := make([]string, len(cont.ActiveContexts))
	copy(clone, cont.ActiveContexts)
	return clone
}

// AddContext will add given context to the ActiveContexts and returns
// true on success.
//
// If the container has already been initialized, nothing will be done and
// false is returned.
func (cont *DefaultContainer) AddContext(context string) bool {
	if cont.isInit {
		return false
	}
	cont.ActiveContexts = append(cont.ActiveContexts, context)
	return true
}

// Size returns the current size of this container.
//
// This is equal of the nodes currently in the depency graph.
// If called before a successful Init, however, this size may
// also includes required parent nodes, that may be registered
// in the future.
func (cont *DefaultContainer) Size() int {
	return len(cont.Nodes)
}

/**
 * Unexported helper methods
 */

// getNodeOrDummy will return a node to given type or a new dummy node if the node does not exist yet
//
// If the node was created, true will be returned
//
// If an error occures, error will be not nil and the other values will be their zero value.
func (cont *DefaultContainer) getNodeOrDummy(t reflect.Type) (*DependencyNode, bool, error) {
	key, err := getPointerType(t)
	if err != nil {
		if isFragile() {
			panic(fmt.Errorf("Failed at pointer conversion: %v", err))
		}
		return nil, false, err
	}
	node := cont.Nodes[key]
	isNew := false
	if node == nil {
		node = NewDepNode()
		node.IsDummy = true
		node.InstanceType = key
		cont.Nodes[key] = node
		isNew = true
	}
	return node, isNew, nil
}

func (cont *DefaultContainer) registerFactoryNode(method *reflect.Value, factory *Factory) error {
	methodType := method.Type()

	parentCount := methodType.NumIn()
	childrenCount := methodType.NumOut()

	if parentCount == 0 && childrenCount == 0 {
		return fmt.Errorf("Cannot register factory: Factory %v neither consumes nor produces anything", method.Type())
	}

	factory.Method = method
	factory.Input = make([]*DependencyNode, parentCount)
	factory.InputTypes = make([]reflect.Type, parentCount)
	factory.Creates = make([]*DependencyNode, childrenCount)
	factory.CreatesTypes = make([]reflect.Type, childrenCount)

	/************** Create nodes that the Creater produces **************/

	// Check if we are allowed to register the creater
	outSet := make(map[reflect.Type]bool)
	for i := 0; i < childrenCount; i++ {
		outType := methodType.Out(i)

		key, err := getPointerType(outType)
		if err != nil {
			return fmt.Errorf("Failed to convert %v to single pointer type: %v", outType, err)
		}

		// Check if there are double output types
		if outSet[key] {
			return fmt.Errorf("'%v' function has duplicate output type '%v'", nameOf(method), outType)
		}
		outSet[key] = true

		node := cont.Nodes[key]
		if node != nil {
			if node.Type != NFactory {
				return fmt.Errorf("%v is already registered with non-factory type: %v",
					node.InstanceType, node.Type)
			}
			if c := factory.Creater; c != nil {
				for _, otherFactory := range node.PotentialFactories {
					if otherFactory.Creater == c {
						return fmt.Errorf(
							"%T is already registerd for %v. Most likely the factory has been registered twice",
							c, node.InstanceType)
					}
				}
			}
		}
	}

	if childrenCount == 0 {
		// Factory does not produces any children, it is just for initializing some static sutff, etc.

		node := NewDepNode()
		node.Type = NInitializer

		node.Factory = factory

		cont.IniterNodes = append(cont.IniterNodes, node)
	}

	// Check each ouput parameter of the Creater. Those are the created instances by
	// this Creater.
	for i := 0; i < childrenCount; i++ {
		outType := methodType.Out(i)

		key, err := getPointerType(outType)
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		node := cont.Nodes[key] // TODO: Optimize double map call by adding nodes to slice in check loop

		if node == nil {
			node = NewDepNode()
			cont.Nodes[key] = node
		}

		node.IsDummy = false
		node.Type = NFactory
		node.PotentialFactories = append(node.PotentialFactories, factory)
		node.InstanceType = outType
		node.FactoryIndex = i

		factory.CreatesTypes[i] = outType
		factory.Creates[i] = node

		if parentCount == 0 {
			// This node does does not have any parent nodes, it is a root node
			cont.RootNodes[outType] = node
		}
	}

	/************** Create/update nodes that the Creater depends on **************/

	// Check each parameter of the create method. Those are the parents of this Creater
	for i := 0; i < parentCount; i++ {
		inType := methodType.In(i)

		parentNode, _, err := cont.getNodeOrDummy(inType)
		if err != nil {
			return err
		}

		factory.InputTypes[i] = inType
		factory.Input[i] = parentNode
	}

	return nil
}

// detectCycle will go through all nodes and check if there are any cyclic dependencies.
//
// Note: This function will ignore IniterNodes as they are always leave nodes with no children.
// Hence they cannot be part of a cycle.
func (cont *DefaultContainer) detectCycle() string {
	// Detect any cyclic dependencies
	stack := make(map[*DependencyNode]tarjanElem)
	for _, node := range cont.Nodes {
		if _, ok := stack[node]; !ok {
			err, _ := detectCycle(stack, node, 0)
			if err != "" {
				return err
			}
		}
	}
	return ""
}

// RebuildGraph will go through every node and build its dependecies and children.
//
// If building the graph fails for any node, an error is retunred and processing is halted.
// All already handled nodes will remain in the graph.
//
// TODO: Add to Container interface?
func (cont *DefaultContainer) RebuildGraph() error {
	// Pick factories for every node
	for _, node := range cont.Nodes {
		_, err := node.PickFactory()
		if err != nil {
			return err
		}
	}

	for _, node := range cont.IniterNodes {
		_, err := node.PickFactory()
		if err != nil {
			return err
		}
	}
	return nil
}
